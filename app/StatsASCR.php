<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class StatsASCR extends Model
{
    protected $table = 'axa_report_ascr';
    protected $hidden = ['year', 'month'];

    public static function getColumnListing()
    {
        return Schema::getColumnListing('axa_report_ascr');
    }
}
