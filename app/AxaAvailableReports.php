<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AxaAvailableReports extends Model
{
    protected $table = 'axa_available_reports';
}
