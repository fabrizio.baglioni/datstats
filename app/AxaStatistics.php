<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AxaStatistics extends Model
{
    protected $table = 'axa_claims_prod_final_custom';
}
