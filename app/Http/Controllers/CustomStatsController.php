<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use stdClass;
use App\AxaStatistics;

use Illuminate\Http\Request;

class CustomStatsController extends Controller
{
    public function index()
    {
        $data = new stdClass();
        $data->minDate = '2020-01-01';
        $data->maxDate = '2020-08-31';
        return view('stats.custom', compact('data'));
    }

    public function show(Request $request)
    {
        $startDate = $request->input('startDate');
        $endDate   = $request->input('endDate');

        $table = AxaStatistics::whereDate('Last_Calculation_Date','>=', $startDate)
            ->whereDate('Last_Calculation_Date','<=', $endDate)
            ->get();

        $columns = Schema::getColumnListing('axa_claims_prod_final_custom');

        $output = implode(";",$columns)."\n";
        foreach ($table as $row) {
            $output.=  implode(";",$row->toArray())."\n";
        }

        $fileName = "CustomExport_"
            .str_replace('-','',$startDate)."_"
            .str_replace('-','',$endDate).
            ".csv";

        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="'.$fileName.'"',
        );

        return Response::make(rtrim($output, "\n"), 200, $headers);

    }
}
