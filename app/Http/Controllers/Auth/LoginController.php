<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use DAT\PostmanDAT\PostmanDAT;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = 'stats/ascr';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /*
    protected function attemptLogin(Request $request)
    {
        $postman = new PostmanDAT();
        $postman->setCustomerNumber($request->customerNumber);
        $postman->setLogin($request->username);
        $postman->setCustomerSignature(getenv('mcCstSgn'));
        $postman->setInterfacePartner(getenv('mcIntPtNum'));
        $postman->setInterfacePartnerSignature(getenv('mcIntPtSqn'));
        $postman->setHost(getenv('mcHost'));

        try {
            $res = $postman->psHasLicense($request->customerNumber, $request->username, $request->password);
        } catch(\Exception $e)
        {
            return false;
            //return $this->sendFailedLoginResponse($request);
        }



        /*
        $user = new \stdClass();
        $user->login = $request->username;
        $user->customerNumber = $request->customerNumber;

        $request->session()->flash('user', json_encode($user));
        $request->session()->flash('res', json_encode($res));
        $request->session()->reflash();
*/
    /*
        //Auth::user()
        //redirect(route('mystats'));
        //dd(Auth::user());
        //dd($request);

        return true;
    }

    public function username()
    {
        return 'username';
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            'customerNumber' => 'required|numeric|min:1000000|max:9999999',
            'username' => 'required',
            'password' => 'required'
        ]);
    }

    /*public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        dd($credentials);
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
    }*/


}
