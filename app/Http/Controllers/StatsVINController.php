<?php

namespace App\Http\Controllers;

use App\AxaAvailableReports;
use App\AxaStatistics;
use App\StatsVIN;
use App\StatsVINpre092020;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Schema;

class StatsVINController extends StatsController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type='VIN')
    {
        //dd(Auth::user());
        return parent::index('VIN');
    }

    public function show($year, $month)
    {
        $table = StatsVIN::where('year','=', $year)
            ->where('month','=', $month)
            ->get();

        $columns = StatsVIN::getVisibleColumnListing();

        $fileName = $year.$month."_VIN_Export.csv";

        return $this->outputToCsv($columns, $table, $fileName);
    }

    public function custom(Request $request)
    {
        $startDate = $request->input('startDate');
        $endDate   = $request->input('endDate');

        $table = StatsVIN::whereDate('date','>=', $startDate)
            ->whereDate('date','<=', $endDate)
            ->get();

        //remove the first 4 column from being visible
        $columns = array_slice(StatsVIN::getColumnListing(), 4);

        $fileName = "CustomExport_VIN_"
            .str_replace('-','',$startDate)."_"
            .str_replace('-','',$endDate).
            ".csv";

        return $this->outputToCsv($columns, $table, $fileName);
    }
}
