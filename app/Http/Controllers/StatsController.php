<?php

namespace App\Http\Controllers;

use App\AxaAvailableReports;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class StatsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');  //with this line, this controller automatically requires authentication to be run!
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type='VIN')
    {
        $availableReports = AxaAvailableReports::where('type','=', $type)
            ->get();

        $data = new \stdClass();
        $data->availableReports = $availableReports;

        $data->title = strtoupper($type);
        $data->type = strtolower($type);
        $data->minDate = self::getMinDate($type);
        $data->maxDate = self::getMaxDate($type);

        return view('stats', compact('data'));
    }

    private static function getMinDate($type)
    {
        $result = AxaAvailableReports::where('type','=', $type)
            ->orderBy('year', 'ASC')
            ->orderBy('month', 'ASC')
            ->first();

        return date("Y-m-d", strtotime($result->year.'-'.$result->month.'-01'));
    }

    private static function getMaxDate($type)
    {
        $result = AxaAvailableReports::where('type','=', $type)
            ->orderBy('year', 'DESC')
            ->orderBy('month', 'DESC')
            ->first();

        return date("Y-m-t", strtotime($result->year.'-'.$result->month.'-01'));
    }

    protected function outputToCsv($columns, $table, $fileName)
    {
        $output = implode(";",$columns)."\n";
        foreach ($table as $row) {
            $output.=  implode(";",$row->toArray())."\n";
        }

        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="'.$fileName.'"',
        );

        return Response::make(rtrim($output, "\n"), 200, $headers);
    }
}
