<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MonthlyStatsController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $months = [
            'Gennaio 2020' => '202001',
            'Febbraio 2020' => '202002',
            'Marzo 2020' => '202003',
            'Aprile 2020' => '202004'
            ];
        return view('stats.mensili', compact('months'));
    }

    public function show($mese)
    {
        echo $mese;
    }

}
