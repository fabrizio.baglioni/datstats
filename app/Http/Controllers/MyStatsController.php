<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyStatsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');  //with this line, this controller automatically requires authentication to be run!
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('mystats');
    }
}
