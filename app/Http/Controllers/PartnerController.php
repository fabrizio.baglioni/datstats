<?php

namespace App\Http\Controllers;

use App\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $table = Partner::get();

        //remove the first 3 column from being visible
        //$columns = array_slice(Partner::getColumnListing(), 3);
        $data = new \stdClass();
        $data->columns = [
            'ID SOL',
            'N.Cliente',
            'Username',
            'Ragione Sociale',
            'MDO €/h',
            'Sc.Italia %',
            'Sc.Estero %',
            'Mat.Cons.',
            'Sc. Extra %',
            'Cessato'
            ];
        //$data->columns = array_slice(Partner::getColumnListing(),1);
        //$data->columns = Partner::getColumnListing();
        $data->table   = $table;

        return view('partners', compact('data'));
    }

}
