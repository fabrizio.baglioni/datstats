<?php

namespace App\Http\Controllers;

use App\StatsASCR;
use Illuminate\Http\Request;

class StatsASCRController extends StatsController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type='ASCR')
    {
        return parent::index('ASCR');
    }

    public function show($year, $month)
    {
        $table = StatsASCR::where('year','=', $year)
            ->where('month','=', $month)
            ->get();

        //remove the first 2 column from being visible
        $columns = array_slice(StatsASCR::getColumnListing(), 2);

        $fileName = $year.$month."_ASCR_Export.csv";

        return $this->outputToCsv($columns, $table, $fileName);
    }

    public function custom(Request $request)
    {
        $startDate = $request->input('startDate');
        $endDate   = $request->input('endDate');

        $table = StatsASCR::whereDate('Last_Calculation_Date','>=', $startDate)
            ->whereDate('Last_Calculation_Date','<=', $endDate)
            ->get();

        //remove the first 2 column from being visible
        $columns = array_slice(StatsASCR::getColumnListing(), 2);

        $fileName = "CustomExport_ASCR_"
            .str_replace('-','',$startDate)."_"
            .str_replace('-','',$endDate).
            ".csv";

        return $this->outputToCsv($columns, $table, $fileName);
    }
}
