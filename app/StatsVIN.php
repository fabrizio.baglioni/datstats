<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class StatsVIN extends Model
{
    protected $table = 'axa_report_vin';
    protected $hidden = ['year', 'month', 'id', 'date_str'];

    public static function getColumnListing()
    {
        return Schema::getColumnListing('axa_report_vin');
    }

    public static function getVisibleColumnListing()
    {
        //4 = first 4 hidden columns
        return array_slice(StatsVIN::getColumnListing(), 4);
    }


}
