<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class Partner extends Model
{
    protected $table = 'axa_anagrafica_partner';
    protected $hidden = ['id'];

    public static function getColumnListing()
    {
        return Schema::getColumnListing('axa_anagrafica_partner');
    }
}
