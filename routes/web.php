<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

//Route::redirect('stats', 'stats/vin')->name('stats');
Route::redirect('stats', 'stats/vin')->name('stats')->middleware('auth');

Route::get('stats/vin',  'StatsVINController@index')->name('VIN')->middleware('auth');
Route::get('stats/vin/{year}/{month}',  'StatsVINController@show')->middleware('auth');
Route::post('stats/vin/custom',  'StatsVINController@custom')->middleware('auth');

Route::get('stats/ascr', 'StatsASCRController@index')->name('ASCR')->middleware('auth');
Route::get('stats/ascr/{year}/{month}',  'StatsASCRController@show')->middleware('auth');
Route::post('stats/ascr/custom',  'StatsASCRController@custom')->middleware('auth');

Route::get('partners',  'PartnerController@index')->name('Partners')->middleware('auth');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/refreshenv', function () {
    $exitCode = Artisan::call('config:cache');
});
