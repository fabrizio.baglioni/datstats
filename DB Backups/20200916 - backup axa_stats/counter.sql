-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: mysqlhost
-- Creato il: Set 16, 2020 alle 18:24
-- Versione del server: 10.1.23-MariaDB
-- Versione PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wedat_db1`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `counter`
--

CREATE TABLE `counter` (
  `project_id` smallint(5) UNSIGNED NOT NULL,
  `counter` varchar(40) NOT NULL,
  `value` int(11) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `counter`
--

INSERT INTO `counter` (`project_id`, `counter`, `value`, `last_update`) VALUES
(1, 'last_processed_offset', 8, '2020-09-12 14:10:21'),
(1, 'rounds', 20677, '2020-09-12 14:10:07'),
(1, 'vin_delta_days_MAX', 60, '2020-06-10 08:18:15'),
(1, 'vin_delta_days_MIN', 10, '2020-06-10 08:18:15');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `counter`
--
ALTER TABLE `counter`
  ADD PRIMARY KEY (`project_id`,`counter`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
