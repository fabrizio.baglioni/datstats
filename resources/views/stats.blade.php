@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-md-center">
            <div class="row">
                <h1 class="display-5">Statistiche {{strtoupper($data->type)}}</h1>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title align-content-center">Estrazione Personalizzata</h5>
                        <h6 class="card-subtitle mb-2 text-muted">seleziona il periodo di riferimento</h6>
                        {{--<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="card-link">Card link</a>
                        <a href="#" class="card-link">Another link</a>--}}
                        <form method="POST" action="{{$data->type}}/custom">
                            @csrf
                            {{--
                                                <div class="input-group input-group-lg">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="startDateLabel">Data Iniziale</span>
                                                    </div>
                                                    <input min="{{$data->minDate}}" max="{{$data->maxDate}}" type="date" class="form-control" aria-label="Sizing example input" aria-describedby="startDateLabel" name="startDate">

                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="endDateLabel">Data Finale</span>
                                                    </div>
                                                    <input min="{{$data->minDate}}" max="{{$data->maxDate}}" type="date" class="form-control" aria-label="Sizing example input" aria-describedby="endDateLabel" name="endDate">

                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                            --}}
                            <div class="form-group">
                                <label for="startDate">Data Iniziale</label>
                                {{-- <input type="email" class="form-control" name="startDate" aria-describedby="emailHelp" placeholder="Data Iniziale"> --}}
                                <input min="{{$data->minDate}}" max="{{$data->maxDate}}" aria-describedby="startDateHelp" type="date" class="form-control" name="startDate">
                                <small id="startDateHelp" class="form-text text-muted">Data iniziale del periodo da estrarre</small>
                            </div>
                            <div class="form-group">
                                <label for="endDate">Data Finale</label>
                                {{-- <input type="email" class="form-control" name="startDate" aria-describedby="emailHelp" placeholder="Data Finale"> --}}
                                <input min="{{$data->minDate}}" max="{{$data->maxDate}}" aria-describedby="endDateHelp" type="date" class="form-control"  name="endDate">
                                <small id="endDateHelp" class="form-text text-muted">Data finale del periodo da estrarre</small>
                            </div>
                            <button type="submit" class="btn btn-primary">Estrai</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Lista estrazioni disponibili</h5>
                        <h6 class="card-subtitle mb-2 text-muted">seleziona un'estrazione dati già consolidata</h6>
                        <div class="row justify-content-md-center" style="margin-top:40px;">
                            <div class="col">
                                <div class="list-group">
                                    @foreach ($data->availableReports as $report)
                                        <a href="{{ url('/stats/'.$data->type, [$report->year, $report->month]) }}" class="list-group-item list-group-item-action">
                                            {{$report->year}} / {{$report->month}}
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


