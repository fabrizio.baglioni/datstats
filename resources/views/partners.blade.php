@extends('layouts.app')

@section('header')
    <link rel="stylesheet" type="text/css" href="DataTables{{$suffix= "3"}}/datatables.min.css"/>

    <script type="text/javascript" src="DataTables{{$suffix}}/datatables.min.js" defer></script>
    {{--

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/b-1.6.3/b-colvis-1.6.3/b-html5-1.6.3/b-print-1.6.3/cr-1.5.2/fh-3.1.7/r-2.2.5/datatables.min.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" defer></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" defer></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/b-1.6.3/b-colvis-1.6.3/b-html5-1.6.3/b-print-1.6.3/cr-1.5.2/fh-3.1.7/r-2.2.5/datatables.min.js" defer></script>
--}}
@endsection

@section('footerscript')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#partners').DataTable({
                "language": {
                    "search": "Cerca:",
                    "emptyTable":     "Nessun dato da mostrare",
                    "info":           "da _START_ a _END_ di _TOTAL_ record totali",
                    "decimal": ",",
                    "lengthMenu":  "Mostra _MENU_ record"
                },
                dom: 'Bfrtipl',
                buttons: [
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        } );
    </script>
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-md-center">
            <h1 class="display-5">Lista Partner rete AXA</h1>
        </div>

        <div class="row justify-content-md-center">

            <table id="partners">
                <thead>
                <tr>
                    @foreach($data->columns as $column)
                        <th>{{$column}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($data->table as $row)
                    <tr>
                        <td>{{$row->id_sol}}</td>
                        <td>{{$row->kundennummer}}</td>
                        <td>{{$row->username}}</td>
                        <td>{{$row->ragione_sociale}}</td>
                        <td>{{$row->manodopera}}</td>
                        <td>{{$row->sc_italia}}</td>
                        <td>{{$row->sc_estero}}</td>
                        <td>{{$row->mat_cons}}</td>
                        <td>{{$row->sc_extra}}</td>
                        <td>{{$row->data_cessazione}}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    @foreach($data->columns as $column)
                        <th>{{$column}}</th>
                    @endforeach
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
