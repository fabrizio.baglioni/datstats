@extends('stats')


@section('statslist')
<form method="POST" action="custom">
    @csrf
    <p class="text-justify">Selezionare il periodo desiderato</p>

    <div class="input-group input-group-lg">
        <div class="input-group-prepend">
            <span class="input-group-text" id="startDateLabel">Data Iniziale</span>
        </div>
        <input min="{{$data->minDate}}" max="{{$data->maxDate}}" type="date" class="form-control" aria-label="Sizing example input" aria-describedby="startDateLabel" name="startDate">
    </div>

    <div class="input-group input-group-lg">
        <div class="input-group-prepend">
            <span class="input-group-text" id="endDateLabel">Data Finale</span>
        </div>
        <input min="{{$data->minDate}}" max="{{$data->maxDate}}" type="date" class="form-control" aria-label="Sizing example input" aria-describedby="endDateLabel" name="endDate">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
