@extends('stats')

@section('statslist')
    <h1 class="display-4">Statistiche ASCR mensili</h1>
<div class="list-group">
    @foreach ($months as $label => $month)
        <a href="{{ route('mensile', $month) }}" class="list-group-item list-group-item-action">
            {{$label}}
        </a>
    @endforeach
</div>
@endsection
